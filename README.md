#LIS 4381 Mobile Application Development and Management Fall 2015 A1

#Alexander Garcia

## Bitbucket Repo Link

[Bitbucket Repo Link](https://bitbucket.org/amg13g/lis4381_fall15 "Bitbucket Repo Link")

## My Website Link

[alexandermiguelgarcia.com](http://alexandermiguelgarcia.com/ "alexandermiguelgarcia.com")

### Ampps Install

![Ampps Install](https://www.bitbucket.org/amg13g/lis4381_fall15/raw/master/a1/images/ampps_install.jpg "Ampps Install") 

### SSH Keygen Information

An SSH keygen produces a public key and an identification. 
These files are saved in a .ssh folder

### Git commands and Descriptions

1. git init - creates a reposetory from a directory
2. git status - see the status of files that are in your staging area to be commited
3. git add - add a file to be commited
4. git commit - records all the files that were added and makes the changes
5. git push - sends the reposetory changes to a remote reposetory
6. git pull - updates a local reposetory from a remote one
7. git log - shows the commit history of a branch